MODULE SP_SC2

USE PARAM,ONLY:LD,NY,NZ,MODEL
USE TYPES

IMPLICIT NONE

REAL(RPREC),DIMENSION(LD,NY) :: LBC_SC2,        &          !DIRICHLET/FLUX BC
                                UBC_SC2

REAL(RPREC),DIMENSION(:,:,:),ALLOCATABLE :: EPSI_SC2


REAL(RPREC),DIMENSION(:,:,:),ALLOCATABLE ::  SC2,                          &
                                      RHS_SC2,RHSF_SC2,             &
                                      DSC2DZ,DSC2DX,DSC2DY,         &
                                      TXSC2,TYSC2,TZSC2,            &
                                      NU_SC2,BETA_SC2,SC2_LAG
REAL(RPREC),DIMENSION(:,:,:),ALLOCATABLE ::  DS_OPT2_SC2

!law of the wall models
REAL(RPREC),DIMENSION(LD,NY) :: DSC2DZMOD,TZSC2MOD

REAL(RPREC),DIMENSION(:,:),ALLOCATABLE :: SC2_STAR,PSI_V,PHI_V
REAL(RPREC),DIMENSION(:,:),ALLOCATABLE :: LBC_SC2_ZO
! Vertical SGS flux of SC2 at specific grid node for eddy covariance sampling output
REAL(RPREC) :: ATQZ_EDDY_COV

CONTAINS

! ALLOCATION
subroutine allocHumidity()

print*,'allocation scalars'

ALLOCATE(EPSI_SC2(LD,NY,0:NZ))
ALLOCATE(SC2(LD,NY,0:NZ))
ALLOCATE(DSC2DX(LD,NY,0:NZ))
ALLOCATE(DSC2DY(LD,NY,0:NZ))
ALLOCATE(DSC2DZ(LD,NY,0:NZ))
ALLOCATE(RHS_SC2(LD,NY,0:NZ))
ALLOCATE(RHSF_SC2(LD,NY,0:NZ))
ALLOCATE(TXSC2(LD,NY,0:NZ))
ALLOCATE(TYSC2(LD,NY,0:NZ))
ALLOCATE(TZSC2(LD,NY,0:NZ))
ALLOCATE(NU_SC2(LD,NY,0:NZ))
ALLOCATE(BETA_SC2(LD,NY,0:NZ))
ALLOCATE(SC2_LAG(LD,NY,0:NZ))
ALLOCATE(DS_OPT2_SC2(LD,NY,NZ))
ALLOCATE(SC2_STAR(LD,NY))
ALLOCATE(PSI_V(LD,NY))
ALLOCATE(PHI_V(LD,NY))
ALLOCATE(LBC_SC2_ZO(LD,NY))

end subroutine allocHumidity


! DEALLOCATION
subroutine deAllocHumidity()

DEALLOCATE(EPSI_SC2)
DEALLOCATE(SC2)
DEALLOCATE(DSC2DX)
DEALLOCATE(DSC2DY)
DEALLOCATE(DSC2DZ)
DEALLOCATE(RHS_SC2)
DEALLOCATE(RHSF_SC2)
DEALLOCATE(TXSC2)
DEALLOCATE(TYSC2)
DEALLOCATE(TZSC2)
DEALLOCATE(NU_SC2)
DEALLOCATE(BETA_SC2)
DEALLOCATE(SC2_LAG)
DEALLOCATE(DS_OPT2_SC2)
DEALLOCATE(SC2_STAR)
DEALLOCATE(PSI_V)
DEALLOCATE(PHI_V)
DEALLOCATE(LBC_SC2_ZO)

end subroutine deAllocHumidity



END MODULE SP_SC2
