module dealias

contains

SUBROUTINE DEALIAS1(U,U_BIG)
! PUTS AN ARRAY INTO THE 'BIG' SIZE
! DOESN'T DEALIAS ANYTHING
! NOTE: THIS SORT OF TRASHES U
USE TYPES, ONLY: RPREC
USE PARAM, ONLY: LD,LD_BIG,NX,NY,NZ,NY2
USE FFT
IMPLICIT NONE

INTEGER :: JZ
REAL(RPREC),DIMENSION(LD,NY,0:NZ),INTENT(IN) :: U
REAL(RPREC), DIMENSION(LD_BIG,NY2,0:NZ),INTENT(OUT) :: U_BIG
REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: TEMP  
REAL(RPREC) :: CONST

CONST=1.0_RPREC/(NX*NY)
TEMP=CONST*U

!$OMP PARALLEL DO
DO JZ=0,NZ
   CALL DFFTW_EXECUTE_DFT_R2C(FORW,TEMP(:,:,JZ),TEMP(:,:,JZ))
   ! CHECK PADD SYNTAX
   CALL PADD(U_BIG(:,:,JZ),TEMP(:,:,JZ))
   CALL DFFTW_EXECUTE_DFT_C2R(BACK_BIG,U_BIG(:,:,JZ),U_BIG(:,:,JZ) )      

ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE DEALIAS1






SUBROUTINE DEALIAS2 (U, U_BIG)
! PUTS BACK INTO SMALL ARIRAY
USE TYPES, ONLY: RPREC
USE PARAM, ONLY: LD,LD_BIG,NY,NZ,NX2,NY2
USE FFT
IMPLICIT NONE

INTEGER :: JZ
REAL(RPREC),DIMENSION(LD,NY,0:NZ),INTENT(OUT) :: U		!LD,NY,0:NZ
REAL(RPREC),DIMENSION(LD_BIG,NY2,0:NZ),INTENT(INOUT) :: U_BIG	!LD_BIG,NY2,0:NZ
REAL(RPREC) :: IGNORE_ME, CONST

!NORMALIZE
CONST=1._RPREC/(NX2*NY2)
U_BIG=CONST*U_BIG

!LOOP THROUGH HORIZONTAL SLICES (important to do from 0 to nz)
!$OMP PARALLEL DO
DO JZ=0,NZ

   !PERFORM FORWARD FFT
   CALL DFFTW_EXECUTE_DFT_R2C( FORW_BIG, U_BIG(:,:,JZ), U_BIG(:,:,JZ) )
   CALL UNPADD(U(:,:,JZ),U_BIG(:,:,JZ))
   !BACK TO PHYSICAL SPACE
   CALL DFFTW_EXECUTE_DFT_C2R( BACK, U(:,:,JZ), U(:,:,JZ) )  

ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE DEALIAS2


END MODULE DEALIAS
