module tkeBudget1Dw

use ifport
use types,only:rp=>rprec
use param
use sim_param
use sp_sc1
use sgsmodule,only:beta,cs_opt2,nu_t
use deriv,only:ddz_w

implicit none

private
character(256) :: oPath
real(rp),dimension(0:nz),save :: au=0.0_rp,av=0.0_rp,aw=0.0_rp,ap=0.0_rp,auu=0.0_rp,avv=0.0_rp,aww=0.0_rp,auv=0.0_rp,auw=0.0_rp,avw=0.0_rp,                      &
                             adudx=0.0_rp,adudy=0.0_rp,adudz=0.0_rp,advdx=0.0_rp,advdy=0.0_rp,advdz=0.0_rp,adwdx=0.0_rp,adwdy=0.0_rp,adwdz=0.0_rp,           &
                             adudx2=0.0_rp,adudy2=0.0_rp,adudz2=0.0_rp,advdx2=0.0_rp,advdy2=0.0_rp,advdz2=0.0_rp,adwdx2=0.0_rp,adwdy2=0.0_rp,adwdz2=0.0_rp,  &
                             atxx=0.0_rp,atyy=0.0_rp,atzz=0.0_rp,atxy=0.0_rp,atxz=0.0_rp,atyz=0.0_rp,         &
                             acs=0.0_rp,abeta=0.0_rp,                                    &
                             auuw=0.0_rp,avvw=0.0_rp,awww=0.0_rp,                        &
                             apw=0.0_rp,apu=0.0_rp,                                      &
                             adpdx=0.0_rp,adpdy=0.0_rp,adpdz=0.0_rp,                     & 
                             apdudx=0.0_rp,apdvdy=0.0_rp,apdwdz=0.0_rp,                  &
                             autxz=0.0_rp,avtyz=0.0_rp,awtzz=0.0_rp,                     & 
                             adxx=0.0_rp,adyy=0.0_rp,adzz=0.0_rp,                        &
                             adxy=0.0_rp,adxz=0.0_rp,adyz=0.0_rp,                        &
                             apdudz=0.0_rp,apdwdx=0.0_rp,auww=0.0_rp,                    &
                             adudxdwdx=0.0_rp,adudydwdy=0.0_rp,adudzdwdz=0.0_rp
real(rp),dimension(0:nz),save :: as=0.0_rp,ass=0.0_rp,aus=0.0_rp,avs=0.0_rp,aws=0.0_rp,aps=0.0_rp,atsx=0.0_rp,atsy=0.0_rp,atsz=0.0_rp,     &
                             ads=0.0_rp,abetas=0.0_rp,apdsdz=0.0_rp,asww=0.0_rp,                                                       &
                             adsdxdwdx=0.0_rp,adsdydwdy=0.0_rp,adsdzdwdz=0.0_rp
public computeTkeBudgetWnodes


!##############################################################################
contains


!##################################################################################



!!------------------------------------------------------------------------------
!!  OBJECT: subroutine ComputeBudget()
!!------------------------------------------------------------------------------
!!
!!  LAST CHECKED/MODIFIED: Marco Giometto ( mgiometto@gmail.com ), day 26/07/2014
!!
!!  DESCRIPTION:
!!
!!  This subroutine extracts the needed statistics for the KAT simulation
!!  all data are interpolated in w nodes
!!
!!
!!  Production is OK
!!  Buoyancy production is also OK
!!  Pressure transport is OK
!!  Transport by molecular diffusion and turbulence is OK
!!  Dissipation is OK
!!  
!!------------------------------------------------------------------------------
subroutine computeTkeBudgetWnodes()

implicit none

integer :: i,j,k

real(rp),dimension(ld,ny,0:nz) :: scr1,scr2,scr3,scr4  !scratch useful for interpolation
character(256) :: fname
logical,save :: init=.false.
integer,save :: ii=-10000
integer :: result


if(.not.init)then
   oPath=trim(out_path)//'ta1_profiles_w_nodes/' 

   !initialize record index
   ii = floor(real(jt_total,rprec)/real(p_cnt3,rprec)) + 1
   if((coord==0).and.(mod(dble(nsteps),dble(p_cnt3)).ne.0.0_rp))then
      print*,'Attention, mod(nsteps,p_cnt3).ne.0.0!'
      print*,'nsteps=',nsteps
      print*,'p_cnt3=',p_cnt3
      print*,'c_cnt3=',c_cnt3
      stop
   endif

   !create folder and write parameters file
   if(coord==0)then
   
      !create output folder
      print*,'Creating folder for output...'
      result=system('mkdir '//trim(oPath))
      if(result.eq.0)then
         print*,'Created output folder!'
      else
         print*,'Output already exists!'
         print*,'Result=',Result
      endif
    
      !write/replace parameters file
      write(fname,'(a)') trim(oPath)//'parameters.txt'
      open(111,file=fname,status='replace')      !simply replaces in case exists
      print*,'Writing parameters for budget...'
      write(111,*) nx
      write(111,*) ny
      write(111,*) nz_tot-1
      write(111,*) l_x
      write(111,*) l_y
      write(111,*) l_z
      write(111,*) dx
      write(111,*) dy
      write(111,*) dz
      write(111,*) nsteps/p_cnt3
      write(111,*) nproc
      write(111,*) Re
      write(111,*) Ri_sc1
      write(111,*) Pr_sc1
      write(111,*) alpha
      if(SGS)then
         write(111,*) 1
      else
         write(111,*) 0
      endif
      if(TEMPERATURE)then
         write(111,*) 1
      else
         write(111,*) 0
      endif
      if(LLBC_SPECIAL=='IBM_DFA')then
         write(111,*) 1
      else
         write(111,*) 0
      endif
      close(111)
   endif   !coord==0
endif      !init

!switch
init=.true.

!mean vars on uvp nodes
call int_var(u,scr1); call avg_var(scr1,au,'u',ii)                                            !vel
call int_var(v,scr2); call avg_var(scr2,av,'v',ii)
call avg_var(w,aw,'w',ii)
if(TEMPERATURE)then
   call int_var(sc1,scr3)
   call avg_var(scr3,as,'s',ii)
endif
call int_var(dudx,scr1); call avg_var(scr1,adudx,'dudx',ii)                                    !vel
call int_var(dudy,scr2); call avg_var(scr2,adudy,'dudy',ii)                                    !vel
call avg_var(dudz,adudz,'dudz',ii)                                                            !vel
call int_var(dvdx,scr1); call avg_var(scr1,advdx,'dvdx',ii)                                    !vel
call int_var(dvdy,scr2); call avg_var(scr2,advdy,'dvdy',ii)                                    !vel
call avg_var(dvdz,advdz,'dvdz',ii)                                                            !vel
call avg_var(dwdx,adwdx,'dwdx',ii)                                                            !vel
call avg_var(dwdy,adwdy,'dwdy',ii)                                                            !vel
call int_var(dwdz,scr1); call avg_var(scr1,adwdz,'dwdz',ii)                                    !vel

call int_var(u,scr1); 
call int_var(v,scr2); 
call avg_var(scr1**2,auu,'uu',ii)                                                                 !res flux
call avg_var(scr2**2,avv,'vv',ii)
call avg_var(w*w,aww,'ww',ii)
call avg_var(scr1*scr2,auv,'uv',ii)
call avg_var(scr1*w,auw,'uw',ii)
call avg_var(scr2*w,avw,'vw',ii)
if(TEMPERATURE)then
   call int_var(sc1,scr3); 
   call avg_var(scr3**2,ass,'ss',ii)    
   call avg_var(scr1*scr3,aus,'su',ii)    
   call avg_var(scr2*scr3,avs,'sv',ii)
   call avg_var(w*scr3,aws,'sw',ii)
endif

call int_var(txx,scr1); call avg_var(scr1,atxx,'txx',ii)                     !sgs flux or dns stress
call int_var(tyy,scr1); call avg_var(scr1,atyy,'tyy',ii)
call int_var(tzz,scr1); call avg_var(scr1,atzz,'tzz',ii)
call int_var(txy,scr1); call avg_var(scr1,atxy,'txy',ii)
call avg_var(txz,atxz,'txz',ii)
call avg_var(tyz,atyz,'tyz',ii)
if(TEMPERATURE)then
   call int_var(txsc1,scr1); 
   call int_var(tysc1,scr2);   
   call avg_var(scr1,atsx,'tsx',ii)
   call avg_var(scr2,atsy,'tsy',ii)
   call avg_var(tzsc1,atsz,'tsz',ii)
endif

if(sgs)then
    scr1(:,:,1:nz)=cs_opt2(:,:,:)
    scr1(:,:,0)=scr1(:,:,1)
    call avg_var(scr1,acs,'cs',ii)                     !les stuff not interpolated
    call avg_var(BETA,abeta,'beta',ii)
    if(TEMPERATURE)then
       scr1(:,:,1:nz)=ds_opt2_sc1(:,:,:)
       scr1(:,:,0)=scr1(:,:,1)   !this way I take care of the fact that Cs is defined in UVP in jz=1
       call avg_var(scr1,ads,'ds',ii)                     !les stuff 
       call avg_var(beta_sc1,abetas,'betas',ii)
    endif
endif

   !turb transport
   call int_var(u,scr1); 
   call int_var(v,scr2); 
   call avg_var(scr1**2*w,auuw,'uuw',ii)
   call avg_var(scr2**2*w,avvw,'vvw',ii)
   call avg_var(w**3,awww,'www',ii)
   call avg_var(scr1*w**2,auww,'uww',ii)
   if(TEMPERATURE)then
      call int_var(sc1,scr3); 
      call avg_var(scr3*w**2,asww,'sww',ii)
   endif 
  !sgs transp
   call int_var(tzz,scr3); 
   call avg_var(w*scr3,awtzz,'wtzz',ii)
   call avg_var(scr1*txz,autxz,'utxz',ii)
   call avg_var(scr2*tyz,avtyz,'vtyz',ii)

   !dissipation (sgs)
   call int_var(dudx*txx,scr1); 
   call avg_var(scr1,adxx,'dxx',ii)
   call int_var(dvdy*tyy,scr1); 
   call avg_var(scr1,adyy,'dyy',ii)
   call int_var(dwdz*tzz,scr1); 
   call avg_var(scr1,adzz,'dzz',ii)
   call int_var(0.5_rp*(dudy+dvdx)*txy,scr1); 
   call avg_var(scr1,adxy,'dxy',ii)
   call avg_var(0.5_rp*(dudz+dwdx)*txz,adxz,'dxz',ii)
   call avg_var(0.5_rp*(dvdz+dwdy)*tyz,adyz,'dyz',ii)

   !dissipation
   call int_var(dudx**2,scr1); 
   call int_var(dudy**2,scr2); 
   call avg_var(scr1,adudx2,'dudx2',ii)
   call avg_var(scr2,adudy2,'dudy2',ii)
   call avg_var(dudz**2,adudz2,'dudz2',ii)
   call int_var(dvdx**2,scr1); 
   call int_var(dvdy**2,scr2); 
   call avg_var(scr1,advdx2,'dvdx2',ii)
   call avg_var(scr2,advdy2,'dvdy2',ii)
   call avg_var(dvdz*dvdz,advdz2,'dvdz2',ii)
   call avg_var(dwdx**2,adwdx2,'dwdx2',ii)
   call avg_var(dwdy**2,adwdy2,'dwdy2',ii)
   call int_var(dwdz**2,scr1); 
   call avg_var(scr1,adwdz2,'dwdz2',ii)
 
   !dissipation (UW)
   call int_var(dudx,scr1)
   call int_var(dudy,scr2)
   call int_var(dwdz,scr3)
   call avg_var(dwdx*scr1,adudxdwdx,'dudxdwdx',ii)
   call avg_var(dwdy*scr2,adudydwdy,'dudydwdy',ii)
   call avg_var(dudz*scr3,adudzdwdz,'dudzdwdz',ii)

if(TEMPERATURE)then
   !dissipation (SW)
   call int_var(dsc1dx,scr1)
   call int_var(dsc1dy,scr2)
   call int_var(dwdz,scr3)
   call avg_var(dwdx*scr1,adsdxdwdx,'dsdxdwdx',ii)
   call avg_var(dwdy*scr2,adsdydwdy,'dsdydwdy',ii)
   call avg_var(scr3*dsc1dz,adsdzdwdz,'dsdzdwdz',ii)
endif

!pressure stuff
call int_var(p,scr2)
call int_var(u,scr3)
call int_var(v,scr4)
scr1=scr2-0.5_rp*(scr3**2+scr4**2+w**2) 

!derivatives
call filt_da(scr1,scr2,scr3)
call avg_var(scr1,ap,'p',ii)    !cleaned pressure
call avg_var(scr2,adpdx,'dpdx',ii)    !cleaned pressure
call avg_var(scr3,adpdy,'dpdy',ii)    !cleaned pressure
call ddz_w(scr2,scr1)
call int_var(scr2,scr3); call avg_var(scr3,adpdz,'dpdz',ii)    !cleaned pressure
call int_var(u,scr2)
call avg_var(scr1*w,apw,'pw',ii)        !pressure transp tke
call avg_var(scr1*scr2,apu,'pu',ii)
if(TEMPERATURE)then
   call int_var(sc1,scr2)
   call avg_var(scr1*scr2,aps,'ps',ii)
endif
call int_var(dudx,scr2)
call int_var(dvdy,scr3)
call int_var(dwdz,scr4)
call avg_var(scr1*scr2,apdudx,'pdudx',ii)
call avg_var(scr1*scr3,apdvdy,'pdvdy',ii)
call avg_var(scr1*scr4,apdwdz,'pdwdz',ii)
call avg_var(scr1*dwdx,apdwdx,'pdwdx',ii)  !pressure transp tke
call avg_var(scr1*dudz,apdudz,'pdudz',ii)  !pressure transp tke
if(TEMPERATURE)then
   call avg_var(scr1*dsc1dz,apdsdz,'pdsdz',ii)  !pressure transp tke
endif

!update record
if(mod(jt,p_cnt3).eq.0)then
   ii=ii+1
endif

end subroutine computeTkeBudgetWnodes


!!------------------------------------------------------------------------------
!!  OBJECT: subroutine avg_var()
!!------------------------------------------------------------------------------
subroutine avg_var(v,av,str_v,ii)

implicit none

real(rp),dimension(ld,ny,0:nz),intent(in) :: v
real(rp),dimension(0:nz),intent(inout) :: av
real(rp),dimension(ld,ny,0:nz) :: v_local
character(*),intent(in) :: str_v
integer,intent(in) :: ii
character(128) :: fname
integer :: i,j,k
real(rp) :: n
real(rp),dimension(1:nz_tot-1) :: avTot
integer,dimension(nproc) :: recvcounts,displs

   do k=1,nz-1
       av(k)=av(k)+sum(v(1:nx,1:ny,k))
   enddo


!save if index is reached
if(mod(jt,p_cnt3).eq.0)then
   avTot=1234567.0_rp
   recvcounts = nz-1
   displs = coord_of_rank*recvcounts
   call mpi_gatherv (av(1),nz-1,mpi_rprec,avTot(1),recvcounts,displs,mpi_rprec,rank_of_coord(0),comm,ierr)
   if(coord==0)then
      n=dble(p_cnt3/c_cnt3)*dble(nx*ny)
      write(fname,'(a,i0)') trim(oPath)//trim(str_v)//'.out'
      open(11,file=trim(fname),access='direct',recl=8*(nz_tot-1))
      write(11,rec=ii) avTot/n
      close(11)
   endif
   av=0.0_rp      !reinitialize averages
endif

end subroutine avg_var


!!------------------------------------------------------------------------------
!!  OBJECT: subroutine int_var()
!!------------------------------------------------------------------------------
!!
!!  LAST CHECKED/MODIFIED: Marco Giometto ( mgiometto@gmail.com ), day 20/01/2014
!!
!!  DESCRIPTION:
!!
!!  This subroutine interpolates from w to uvp
!!
!!------------------------------------------------------------------------------
subroutine int_var(v,iv)

implicit none

real(rp),dimension(ld,ny,0:nz),intent(in) :: v
real(rp),dimension(ld,ny,0:nz),intent(out) :: iv
integer :: k

!inteprolate from uvp to w
do k=1,nz-1
   iv(:,:,k)=0.5_rp*(v(:,:,k)+v(:,:,k-1))
enddo

end subroutine int_var


end module tkeBudget1Dw
