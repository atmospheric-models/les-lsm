module tkeBudget1D

use ifport
use types,only:rp=>rprec
use param
use sim_param
use sp_sc1
use sgsmodule,only:beta,cs_opt2,nu_t
use deriv,only:ddz_uv

implicit none

private
character(256) :: oPath
real(rp),dimension(0:nz),save :: au=0.0_rp,av=0.0_rp,aw=0.0_rp,ap=0.0_rp,auu=0.0_rp,avv=0.0_rp,aww=0.0_rp,auv=0.0_rp,auw=0.0_rp,avw=0.0_rp,                      &
                             adudx=0.0_rp,adudy=0.0_rp,adudz=0.0_rp,advdx=0.0_rp,advdy=0.0_rp,advdz=0.0_rp,adwdx=0.0_rp,adwdy=0.0_rp,adwdz=0.0_rp,           &
                             adudx2=0.0_rp,adudy2=0.0_rp,adudz2=0.0_rp,advdx2=0.0_rp,advdy2=0.0_rp,advdz2=0.0_rp,adwdx2=0.0_rp,adwdy2=0.0_rp,adwdz2=0.0_rp,  &
                             atxx=0.0_rp,atyy=0.0_rp,atzz=0.0_rp,atxy=0.0_rp,atxz=0.0_rp,atyz=0.0_rp,         &
                             acs=0.0_rp,abeta=0.0_rp,                                    &
                             auuw=0.0_rp,avvw=0.0_rp,awww=0.0_rp,                        &
                             apw=0.0_rp,apu=0.0_rp,                                      &
                             adpdx=0.0_rp,adpdy=0.0_rp,adpdz=0.0_rp,                     & 
                             apdudx=0.0_rp,apdvdy=0.0_rp,apdwdz=0.0_rp,                  &
                             autxz=0.0_rp,avtyz=0.0_rp,awtzz=0.0_rp,                     & 
                             adxx=0.0_rp,adyy=0.0_rp,adzz=0.0_rp,                        &
                             adxy=0.0_rp,adxz=0.0_rp,adyz=0.0_rp,                        &
                             apdudz=0.0_rp,apdwdx=0.0_rp,auww=0.0_rp,                    &
                             adudxdwdx=0.0_rp,adudydwdy=0.0_rp,adudzdwdz=0.0_rp
real(rp),dimension(0:nz),save :: as=0.0_rp,ass=0.0_rp,aus=0.0_rp,avs=0.0_rp,aws=0.0_rp,aps=0.0_rp,atsx=0.0_rp,atsy=0.0_rp,atsz=0.0_rp,     &
                                 adsdx=0.0_rp,adsdy=0.0_rp,adsdz=0.0_rp,ads=0.0_rp,abetas=0.0_rp,apdsdz=0.0_rp,asww=0.0_rp,                &
                                 adsdxdwdx=0.0_rp,adsdydwdy=0.0_rp,adsdzdwdz=0.0_rp
public computeTkeBudget


!##############################################################################
contains


!##################################################################################



!!------------------------------------------------------------------------------
!!  OBJECT: subroutine ComputeBudget()
!!------------------------------------------------------------------------------
!!
!!  LAST CHECKED/MODIFIED: Marco Giometto ( mgiometto@gmail.com ), day 26/07/2014
!!
!!  DESCRIPTION:
!!
!!  This subroutine extracts the needed statistics for the KAT simulation
!!  all data are interpolated in w nodes
!!
!!
!!  Production is OK
!!  Buoyancy production is also OK
!!  Pressure transport is OK
!!  Transport by molecular diffusion and turbulence is OK
!!  Dissipation is OK
!!  
!!------------------------------------------------------------------------------
subroutine computeTkeBudget()

implicit none

integer :: i,j,k

real(rp),dimension(ld,ny,0:nz) :: scr1,scr2,scr3,scr4  !scratch useful for interpolation
character(256) :: fname
logical,save :: init=.false.
integer,save :: ii=-10000
integer :: result


if(.not.init)then
   oPath=trim(out_path)//'ta1_profiles/' 

   !initialize record index
   ii=floor(real(jt_total,rprec)/real(p_cnt3,rprec)) + 1
   if((coord==0).and.(mod(dble(nsteps),dble(p_cnt3)).ne.0.0_rp))then
      print*,'Attention, mod(nsteps,p_cnt3).ne.0.0!'
      print*,'nsteps=',nsteps
      print*,'p_cnt3=',p_cnt3
      print*,'c_cnt3=',c_cnt3
      stop
   endif

   !create folder and write parameters file
   if(coord==0)then
   
      !create output folder
      print*,'Creating folder for output...'
      result=system('mkdir '//trim(oPath))
      if(result.eq.0)then
         print*,'Created output folder!'
      else
         print*,'Output already exists!'
         print*,'Result=',Result
      endif
    
      !write/replace parameters file
      write(fname,'(a)') trim(oPath)//'parameters.txt'
      open(111,file=fname,status='replace')      !simply replaces in case exists
      print*,'Writing parameters for budget...'
      write(111,*) nx
      write(111,*) ny
      write(111,*) nz_tot-1
      write(111,*) l_x
      write(111,*) l_y
      write(111,*) l_z
      write(111,*) dx
      write(111,*) dy
      write(111,*) dz
      write(111,*) nsteps/p_cnt3
      write(111,*) nproc
      write(111,*) Re
      write(111,*) Ri_sc1
      write(111,*) Pr_sc1
      write(111,*) alpha
      if(SGS)then
         write(111,*) 1
      else
         write(111,*) 0
      endif
      if(TEMPERATURE)then
         write(111,*) 1
      else
         write(111,*) 0
      endif
      if(LLBC_SPECIAL=='IBM_DFA')then
         write(111,*) 1
      else
         write(111,*) 0
      endif
      write(111,*)  phi_rot
      close(111)
   endif   !coord==0
endif      !init

!switch
init=.true.

!mean vars on uvp nodes
call avg_var(u,au,'u',ii)                                                            !vel
call avg_var(v,av,'v',ii)
call int_var(w,scr1); call avg_var(scr1,aw,'w',ii)
if(TEMPERATURE) call avg_var(sc1,as,'s',ii)                                               !vel

call avg_var(dudx,adudx,'dudx',ii)                                                            !vel
call avg_var(dudy,adudy,'dudy',ii)                                                            !vel
call int_var(dudz,scr2); call avg_var(scr2,adudz,'dudz',ii)                                   !vel
call avg_var(dvdx,advdx,'dvdx',ii)                                                            !vel
call avg_var(dvdy,advdy,'dvdy',ii)                                                            !vel
call int_var(dvdz,scr2); call avg_var(scr2,advdz,'dvdz',ii)                                   !vel
call int_var(dwdx,scr2);
call int_var(dwdy,scr3);
call avg_var(scr2,adwdx,'dwdx',ii)                                                            !vel
call avg_var(scr3,adwdy,'dwdy',ii)                                                            !vel
call avg_var(dwdz,adwdz,'dwdz',ii)                                                            !vel
if(TEMPERATURE)then
   call avg_var(dsc1dx,adsdx,'dsdx',ii)                                                            !vel
   call avg_var(dsc1dy,adsdy,'dsdy',ii)                                                            !vel
   call int_var(dsc1dz,scr2);
   call avg_var(scr2,adsdz,'dsdz',ii)                                                            !vel
endif

call avg_var(u*u,auu,'uu',ii)                                                        !res flux
call avg_var(v*v,avv,'vv',ii)
call avg_var(scr1*scr1,aww,'ww',ii)
call avg_var(u*v,auv,'uv',ii)
call avg_var(scr1*u,auw,'uw',ii)
call avg_var(scr1*v,avw,'vw',ii)
if(TEMPERATURE)then
   call avg_var(sc1*sc1,ass,'ss',ii)    
   call avg_var(u*sc1,aus,'su',ii)    
   call avg_var(v*sc1,avs,'sv',ii)
   call avg_var(scr1*sc1,aws,'sw',ii)
endif

call avg_var(txx,atxx,'txx',ii)                     !sgs flux or dns stress
call avg_var(tyy,atyy,'tyy',ii)
call avg_var(tzz,atzz,'tzz',ii)
call avg_var(txy,atxy,'txy',ii)
call int_var(txz,scr1); call avg_var(scr1,atxz,'txz',ii)
call int_var(tyz,scr1); call avg_var(scr1,atyz,'tyz',ii)

if(TEMPERATURE) call avg_var(txsc1,atsx,'tsx',ii)
if(TEMPERATURE) call avg_var(tysc1,atsy,'tsy',ii)
if(TEMPERATURE)then
   call int_var(tzsc1,scr1); call avg_var(scr1,atsz,'tsz',ii)
endif

if(sgs)then
    scr1(:,:,1:nz)=cs_opt2(:,:,:)
    scr1(:,:,0)=scr1(:,:,1)
    call avg_var(scr1,acs,'cs',ii)                     !les stuff not interpolated
    call avg_var(BETA,abeta,'beta',ii)
    if(TEMPERATURE)then
       scr1(:,:,1:nz)=ds_opt2_sc1(:,:,:)
       scr1(:,:,0)=scr1(:,:,1)   !this way I take care of the fact that Cs is defined in UVP in jz=1
       call avg_var(scr1,ads,'ds',ii)                     !les stuff 
       call avg_var(beta_sc1,abetas,'betas',ii)
    endif
endif

   !turb transport
   call int_var(w,scr1); call avg_var(u**2*scr1,auuw,'uuw',ii)
   call avg_var(v**2*scr1,avvw,'vvw',ii)
   call avg_var(scr1**3,awww,'www',ii)
   call avg_var(u*scr1**2,auww,'uww',ii)
   if(TEMPERATURE) call avg_var(sc1*scr1**2,asww,'sww',ii)
   !sgs transp
   call avg_var(scr1*tzz,awtzz,'wtzz',ii)
   call int_var(txz,scr3); 
   if((coord==0).and.(llbc_special.eq.'WALL_LAW')) scr3(:,:,1)=txz(:,:,1)
   call avg_var(scr3*u,autxz,'utxz',ii)
   call int_var(tyz,scr2); 
   if((coord==0).and.(llbc_special.eq.'WALL_LAW')) scr2(:,:,1)=tyz(:,:,1)
   call avg_var(scr2*v,avtyz,'vtyz',ii)

   !dissipation (sgs)
   call avg_var(dudx*txx,adxx,'dxx',ii)
   call avg_var(dvdy*tyy,adyy,'dyy',ii)
   call avg_var(dwdz*tzz,adzz,'dzz',ii)
   call avg_var(0.5_rp*(dudy+dvdx)*txy,adxy,'dxy',ii)
   call int_var(dudz+dwdx,scr4); 
   if((coord==0).and.(llbc_special.eq.'WALL_LAW')) scr4(:,:,1)=dudz(:,:,1)+0.5*(dwdx(:,:,1)+dwdx(:,:,2))
   call avg_var(0.5_rp*scr4*scr3,adxz,'dxz',ii)
   call int_var(dvdz+dwdy,scr1); 
   if((coord==0).and.(llbc_special.eq.'WALL_LAW')) scr1(:,:,1)=dvdz(:,:,1)+0.5*(dwdy(:,:,1)+dwdy(:,:,2))
   call avg_var(0.5*scr2*scr1,adyz,'dyz',ii)

   !dissipation
   call int_var(dudz,scr1); 
   call int_var(dvdz,scr2); 
   call int_var(dwdx,scr3); 
   call int_var(dwdy,scr4); 
   call avg_var(dudx*dudx,adudx2,'dudx2',ii)
   call avg_var(dudy*dudy,adudy2,'dudy2',ii)
   call avg_var(scr1*scr1,adudz2,'dudz2',ii)
   call avg_var(dvdx*dvdx,advdx2,'dvdx2',ii)
   call avg_var(dvdy*dvdy,advdy2,'dvdy2',ii)
   call avg_var(scr2*scr2,advdz2,'dvdz2',ii)
   call avg_var(scr3*scr3,adwdx2,'dwdx2',ii)
   call avg_var(scr4*scr4,adwdy2,'dwdy2',ii)
   call avg_var(dwdz*dwdz,adwdz2,'dwdz2',ii)
 
   !dissipation (UW)
   call int_var(dwdx,scr1)
   call int_var(dwdy,scr2)
   call int_var(dudz,scr3)
   call avg_var(dudx*scr1,adudxdwdx,'dudxdwdx',ii)
   call avg_var(dudy*scr2,adudydwdy,'dudydwdy',ii)
   call avg_var(scr3*dwdz,adudzdwdz,'dudzdwdz',ii)

if(TEMPERATURE)then
   !dissipation (SW)
   call int_var(dwdx,scr1)
   call int_var(dwdy,scr2)
   call int_var(dsc1dz,scr3)
   call avg_var(dsc1dx*scr1,adsdxdwdx,'dsdxdwdx',ii)
   call avg_var(dsc1dy*scr2,adsdydwdy,'dsdydwdy',ii)
   call avg_var(scr3*dwdz,adsdzdwdz,'dsdzdwdz',ii)
endif

!pressure stuff
do k=1,nz-1
  scr1(:,:,k)=p(:,:,k)-0.5_rp*(u(:,:,k)**2+v(:,:,k)**2+(0.5_rp*(w(:,:,k)+w(:,:,k+1)))**2) 
enddo
!derivatives
call filt_da(scr1,scr2,scr3)
call avg_var(scr1,ap,'p',ii)    !cleaned pressure
call avg_var(scr2,adpdx,'dpdx',ii)    !cleaned pressure
call avg_var(scr3,adpdy,'dpdy',ii)    !cleaned pressure
call ddz_uv(scr2,scr1)
call int_var(scr2,scr3); call avg_var(scr3,adpdz,'dpdz',ii)    !cleaned pressure
call int_var(w,scr2)
call avg_var(scr1*scr2,apw,'pw',ii)        !pressure transp tke
call avg_var(scr1*u,apu,'pu',ii)
if(TEMPERATURE) call avg_var(scr1*sc1,aps,'ps',ii)
call avg_var(scr1*dudx,apdudx,'pdudx',ii)
call avg_var(scr1*dvdy,apdvdy,'pdvdy',ii)
call avg_var(scr1*dwdz,apdwdz,'pdwdz',ii)
call int_var(dudz,scr2)
call int_var(dwdx,scr3)
call avg_var(scr1*scr3,apdwdx,'pdwdx',ii)  !pressure transp tke
call avg_var(scr1*scr2,apdudz,'pdudz',ii)  !pressure transp tke
if(TEMPERATURE)then
   call int_var(dsc1dz,scr4)
   call avg_var(scr1*scr4,apdsdz,'pdsdz',ii)  !pressure transp tke
endif
!update record
if(mod(jt,p_cnt3).eq.0)then
   ii=ii+1
endif

end subroutine computeTkeBudget


!!------------------------------------------------------------------------------
!!  OBJECT: subroutine avg_var()
!!------------------------------------------------------------------------------
subroutine avg_var(v,av,str_v,ii)

implicit none

real(rp),dimension(ld,ny,0:nz),intent(in) :: v
real(rp),dimension(0:nz),intent(inout) :: av
character(*),intent(in) :: str_v
integer,intent(in) :: ii
character(128) :: fname
integer :: i,j,k
real(rp) :: n
real(rp),dimension(1:nz_tot-1) :: avTot
integer,dimension(nproc) :: recvcounts,displs

   do k=1,nz-1
       av(k)=av(k)+sum(v(1:nx,1:ny,k))
   enddo

!save if index is reached
if(mod(jt,p_cnt3).eq.0)then
   avTot=1234567.0_rp
   recvcounts = nz-1
   displs = coord_of_rank*recvcounts
   call mpi_gatherv (av(1),nz-1,mpi_rprec,avTot(1),recvcounts,displs,mpi_rprec,rank_of_coord(0),comm,ierr)
   if(coord==0)then
      n=dble(p_cnt3/c_cnt3)*dble(nx*ny)
      write(fname,'(a,i0)') trim(oPath)//trim(str_v)//'.out'
      open(11,file=trim(fname),access='direct',recl=8*(nz_tot-1))
      write(11,rec=ii) avTot/n
      close(11)
   endif
   av=0.0_rp      !reinitialize averages
endif

end subroutine avg_var


!!------------------------------------------------------------------------------
!!  OBJECT: subroutine int_var()
!!------------------------------------------------------------------------------
!!
!!  LAST CHECKED/MODIFIED: Marco Giometto ( mgiometto@gmail.com ), day 20/01/2014
!!
!!  DESCRIPTION:
!!
!!  This subroutine interpolates from w to uvp
!!
!!------------------------------------------------------------------------------
subroutine int_var(v,iv)

implicit none

real(rp),dimension(ld,ny,0:nz),intent(in) :: v
real(rp),dimension(ld,ny,0:nz),intent(out) :: iv
integer :: k

!inteprolate
do k=0,nz-1
   iv(:,:,k)=0.5_rp*(v(:,:,k)+v(:,:,k+1))
enddo

end subroutine int_var


end module tkeBudget1D
