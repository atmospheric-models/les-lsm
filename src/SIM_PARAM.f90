MODULE SIM_PARAM

USE TYPES
USE PARAM

IMPLICIT NONE

SAVE
PUBLIC      !STANDARD IF NOT SPECIFIED

REAL(RPREC) :: DT,TT

REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: U, V, W,                &
                                           DUDX, DUDY, DUDZ,       &   !could remove all gradients
                                            DVDX, DVDY, DVDZ,       &
                                            DWDX, DWDY, DWDZ,       &
                                            RHSX, RHSY, RHSZ,       &
                                            RHSX_F, RHSY_F, RHSZ_F

REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: P

REAL(RPREC),DIMENSION(LD,NY,1:NZ) :: DPDX,            &
                                     DPDY,            &
                                     DPDZ

REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: TXX,TXY,TYY,TXZ,TYZ,TZZ            !could remove

REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: DIVTX,DIVTY,DIVTZ                  !could remove

REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: div_vel_vs

REAL(RPREC),DIMENSION(LD,NY) :: LBC_U,LBC_V,LBC_W,UBC_U,UBC_V,UBC_W

!REAL(RPREC),DIMENSION(NZ_TOT-1) :: DZ

!filter specification
INTEGER,PARAMETER :: FILTER_SIZE=1
REAL(RPREC),DIMENSION(LH,NY) :: G_TEST=0.0_RPREC,G_TEST_TEST=0.0_RPREC


!law of the wall models
REAL(RPREC),DIMENSION(LD,NY) :: DUDZMOD,DVDZMOD,TXZMOD,TYZMOD

!timing
INTEGER,DIMENSION(8) :: T1A,T2A,T3A,T4A,T5A,T6A,T7A,T8A,T9A,    &
                        T1B,T2B,T3B,T4B,T5B,T6B,T7B,T8B,T9B
INTEGER :: TIME1,TIME2,TIME3,TIME4,TIME5,TIME6,TIME7,TIME8,TIME9

REAL(RPREC),DIMENSION(LD,NY,0:NZ) :: BUOYANCY

REAL(RPREC),DIMENSION(LD,NY) :: USTAR_AVG
REAL(RPREC),DIMENSION(LD,NY) :: PSI_M,PHI_M
REAL(RPREC),DIMENSION(LD,NY) :: LBC_Z0

REAL(RPREC),DIMENSION(LD,NY) :: OBK_L

!!!!!! FOR WIND FARMS

real(rprec),dimension(:,:),allocatable :: turbine_location
real(rprec),dimension(:,:),allocatable :: turbine_normal
real(rprec),dimension(:,:),allocatable :: turbine_inflow_velocity
real(rprec) :: turbine_radius
real(rprec) :: running_theta

!real(rprec),dimension(nx,ny,nz_tot-1) :: force_x,force_y,force_z
real(rprec),dimension(:,:),allocatable :: force_all,force_global,loc_torque
real(rprec),dimension(:),allocatable :: x,y,z,area,radial_distance
real(rprec),dimension(:),allocatable :: temp_u,temp_v
real(rprec),dimension(:),allocatable :: temp_x,temp_y,temp_z,loc_power
real(rprec),dimension(2) :: time_disk_avg_velo
real(rprec) :: velo_for_force_calc
real(rprec) :: up_dist

real(rprec),dimension(3,LD,NY,0:NZ) :: NORM
real(rprec),dimension(LD,NY,0:NZ) :: PHI

! !MOMENTUM
! SUBROUTINE ALLOCATE_MOM()
! 
! IMPLICIT NONE
! 
! ALLOCATE(U(LD,NY,0:NZ))
! ALLOCATE(V(LD,NY,0:NZ))
! ALLOCATE(W(LD,NY,0:NZ))
! 
! ALLOCATE(DUDX(LD,NY,0:NZ))
! ALLOCATE(DUDY(LD,NY,0:NZ))
! ALLOCATE(DUDZ(LD,NY,0:NZ))
! ALLOCATE(DVDX(LD,NY,0:NZ))
! ALLOCATE(DVDY(LD,NY,0:NZ))
! ALLOCATE(DVDZ(LD,NY,0:NZ))
! ALLOCATE(DWDX(LD,NY,0:NZ))
! ALLOCATE(DWDY(LD,NY,0:NZ))
! ALLOCATE(DWDZ(LD,NY,0:NZ))
! 
! ALLOCATE(RHSX(LD,NY,0:NZ))
! ALLOCATE(RHSY(LD,NY,0:NZ))
! ALLOCATE(RHSZ(LD,NY,0:NZ))
! ALLOCATE(RHSX_F(LD,NY,0:NZ))
! ALLOCATE(RHSY_F(LD,NY,0:NZ))
! ALLOCATE(RHSZ_F(LD,NY,0:NZ))
! 
! ALLOCATE(P(LD,NY,0:NZ))
! 
! ALLOCATE(DPDX(LD,NY,NZ))
! ALLOCATE(DPDY(LD,NY,NZ))
! ALLOCATE(DPDZ(LD,NY,NZ))
! 
! ALLOCATE(TXX(LD,NY,0:NZ))
! ALLOCATE(TXY(LD,NY,0:NZ))
! ALLOCATE(TXZ(LD,NY,0:NZ))
! ALLOCATE(TYY(LD,NY,0:NZ))
! ALLOCATE(TYZ(LD,NY,0:NZ))
! ALLOCATE(TZZ(LD,NY,0:NZ))
! 
! ALLOCATE(DIVTX(LD,NY,0:NZ))
! ALLOCATE(DIVTY(LD,NY,0:NZ))
! ALLOCATE(DIVTZ(LD,NY,0:NZ))
! 
! 
! IF(IFEM)THEN
!   ALLOCATE(FX_ICM(LD,NY,0:NZ))
!   ALLOCATE(FY_ICM(LD,NY,0:NZ))
!   ALLOCATE(FZ_ICM(LD,NY,0:NZ))
! ENDIF
! 
! IF(LLBC_SPECIAL.EQ.'IBM_DFA')THEN
!   ALLOCATE(FX(LD,NY,0:NZ))
!   ALLOCATE(FY(LD,NY,0:NZ))
!   ALLOCATE(FZ(LD,NY,0:NZ))
! ENDIF
! 
! END SUBROUTINE ALLOCATE_MOM
! 
! 
! SUBROUTINE DEALLOCATE_MOM()
! 
! IMPLICIT NONE
! 
! DEALLOCATE(U)
! DEALLOCATE(V)
! DEALLOCATE(W)
! 
! DEALLOCATE(DUDX)
! DEALLOCATE(DUDY)
! DEALLOCATE(DUDZ)
! DEALLOCATE(DVDX)
! DEALLOCATE(DVDY)
! DEALLOCATE(DVDZ)
! DEALLOCATE(DWDX)
! DEALLOCATE(DWDY)
! DEALLOCATE(DWDZ)
! 
! DEALLOCATE(RHSX)
! DEALLOCATE(RHSY)
! DEALLOCATE(RHSZ)
! DEALLOCATE(RHSX_F)
! DEALLOCATE(RHSY_F)
! DEALLOCATE(RHSZ_F)
! 
! DEALLOCATE(P)
! 
! DEALLOCATE(DPDX)
! DEALLOCATE(DPDY)
! DEALLOCATE(DPDZ)
! 
! DEALLOCATE(TXX)
! DEALLOCATE(TXY)
! DEALLOCATE(TXZ)
! DEALLOCATE(TYY)
! DEALLOCATE(TYZ)
! DEALLOCATE(TZZ)
! 
! DEALLOCATE(DIVTX)
! DEALLOCATE(DIVTY)
! DEALLOCATE(DIVTZ)
! 
! IF(IFEM)THEN
!   DEALLOCATE(FX_ICM)
!   DEALLOCATE(FY_ICM)
!   DEALLOCATE(FZ_ICM)
! ENDIF
! 
! IF(LLBC_SPECIAL.EQ.'IBM_DFA')THEN
!   DEALLOCATE(FX)
!   DEALLOCATE(FY)
!   DEALLOCATE(FZ)
! ENDIF
! 
! END SUBROUTINE DEALLOCATE_MOM



END MODULE SIM_PARAM
